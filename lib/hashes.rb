# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  arr_sentence = str.split(" ")
  hash =  Hash.new (0)
  arr_sentence.each do |el|
    hash[el] = el.length
  end
  hash
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  hash.sort_by do |k,v|
    v
  end.last.first
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each do |k,v|
    older[k] = v
  end
  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  hash = Hash.new(0)
  arr_word = word.split("")
  arr_word.each do |el|
    hash[el] = word.count(el)
  end
  hash
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  hash = Hash.new(0)
  arr.each_with_index do |el,idx|
    hash[el] = idx
  end
  hash.keys

end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  hash = Hash.new(0)
  hash[:even] = numbers.count { |el| el % 2 == 0  }
  hash[:odd] = numbers.count { |el| el % 2 != 0  }
  hash
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  hash = Hash.new(0)
  arr_str = string.split("")

  arr_str.each do |el|
    hash[el] = string.count(el)
  end
  mosttime = hash.sort_by {|k,v| v}.last.last

 hash.select do |k,v|
   v == mosttime
 end.sort_by {|k,v| k }.first.first



end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  students_new = students.select do |student, month|
   month >= 7
 end

 names = students_new.keys
 result = []

 names.combination(2).to_a
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  hash = Hash.new()
  specimens.each do |el|
    hash[el] = specimens.count(el)
  end
  number_of_species = hash.keys.count
  smallest_population_size = hash.values.min
  largest_population_size = hash.values.max

  (number_of_species ** 2 * smallest_population_size)/largest_population_size
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  normal_count = character_count(normal_sign)
#  p normal_count

vandalized_count = character_count(vandalized_sign)

vandalized_count.all? do |character, count|
  normal_count[character.downcase] >= count
end
end
def character_count(str)
  count = Hash.new(0)
  str.each_char do |char|
    next if char == " "
    count[char.downcase] +=1
  end
  count
end
